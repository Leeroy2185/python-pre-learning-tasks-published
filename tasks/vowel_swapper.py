def vowel_swapper(string):
    # ==============
    if "a".lower() in string:
        string = string.replace("a".lower(), "4")
    if "a".upper() in string:
        string = string.replace("a".upper(), "4")
    if "e".lower() in string:
        string = string.replace("e".lower(), "3")
    if "e".upper() in string:
        string = string.replace("e".upper(), "3")
    if "i".lower() in string:
        string = string.replace("i".lower(), "!")
    if "i".upper() in string:
        string = string.replace("i".upper(), "!")
    if "o".lower() in string:
        string = string.replace("o".lower(), "ooo")
    if "o".upper() in string:
        string = string.replace("o".upper(), "000")

    return (string)

    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
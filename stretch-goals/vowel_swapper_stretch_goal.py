def vowel_swapper(string):
    # ==============
   
    if "a".lower() in string:
        string = string.replace("a".lower(), "4")
    if "a".upper() in string:
        string = string.replace("a".upper(), "4")
    if "e".lower() in string:
        string = string.replace("e".lower(), "3")
    if "e".upper() in string:
        string = string.replace("e".upper(), "3")
    if "i".lower() in string:
        string = string.replace("i".lower(), "!")
    if "i".upper() in string:
        string = string.replace("i".upper(), "!")
    if "o".lower() in string:
        string = string.replace("o".lower(), "ooo")
    if "o".upper() in string:
        string = string.replace("o".upper(), "000")
    if "u".lower() in string:
        string = string.replace("u".lower(), "|_|")
    if "u".upper() in string:
        string = string.replace("u".upper(), "|_|")
    return (string)

    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
